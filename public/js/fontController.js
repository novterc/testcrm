var fontConfig = {
	size: {
		storageName: 'fontSize',
		default: 200,
		step: 30,
	},
	space: {
		storageName: 'fontSpace',
		default: 0,
		step: 0.4,
	},
	color: {
		storageName: 'colorMode',
		default: false,
	}
};


var fontSize = new ClassIntegerOption(fontConfig.size, function(value){
	$('body').css('font-size', value+'%');
});

var fontSpace = new ClassIntegerOption(fontConfig.space, function(value){
	$('body').css('letter-spacing', value+'px');
});

var colorMode = new ClassBooleanOption(fontConfig.color, function(){
	$('body').addClass('dark-mod');
}, function(){
	$('body').removeClass('dark-mod');
})

fontSize.init();
fontSpace.init();
colorMode.init();
	

$('document').ready( function(){


	$('#font-size-plus').on('click', function(){
		fontSize.change(true);
	});

	$('#font-size-minus').on('click', function(){
		fontSize.change(false);
	});

	$('#font-space-plus').on('click', function(){
		fontSpace.change(true);
	});

	$('#font-space-minus').on('click', function(){
		fontSpace.change(false);
	});

	$('#font-dark-on').on('click', function(){
		colorMode.change(true);
	})

	$('#font-dark-off').on('click', function(){
		colorMode.change(false);
	})

	$('#font-default').on('click', function(){
		fontSize.default();
		fontSpace.default();
		colorMode.default();
	})

});

function ClassBooleanOption(config, onFunction, offFunction) {
	this.config = config;
	this.onFunction = onFunction;
	this.offFunction = offFunction;

	this.getStorge = function (){
		var value = localStorage.getItem(config.storageName);
		if( value == null )
			return null;

		if( value == 'true' )
			return true;
		else
			return false;
	}

	this.setStorge = function (value){
		localStorage.setItem(config.storageName, value);
	}

	this.init = function() {
		var value = this.getStorge();	
		if(value == null){
			value = config.default;
			this.setStorge(value);
		}
		this.change(value);
	}

	this.default = function() {
		value = config.default;
		this.setStorge(value);
		this.change(value);
	}

	this.change = function(value) {
		if(value)
			this.onFunction();
		else
			this.offFunction();

		this.setStorge(value);
	}
}

function ClassIntegerOption(config, aplayFunction) {
	this.config = config;
	this.aplay = aplayFunction;

	this.getStorge = function (){
		var value = localStorage.getItem(config.storageName);
		return Number(value);
	}

	this.setStorge = function (value){
		localStorage.setItem(config.storageName, value);
	}

	this.init = function() {
		var value = this.getStorge();	
		if(value == 0){
			value = config.default;
			this.setStorge(value);
		}
		this.aplay(value);
	}

	this.default = function() {
		value = config.default;
		this.setStorge(value);
		this.aplay(value);
	}

	this.change = function(polarity) {
		var value = this.getStorge();
		if(polarity)
			value += config.step;
		else
			value -= config.step;

		this.aplay(value);
		this.setStorge(value);
	}
}
