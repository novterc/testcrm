angular.module( 'ordersApp', [] )
    .controller( 'OrdersController', function( $scope, $http ) {
        var orderApp = this;

        orderApp.action = angular.element($('#orders-search-form')).attr('action');
        orderApp.items = [];

        $scope.form = {
            paginate: '100',
            page: 1,
            sorts: {},
        };
        $scope.lastPage = 0;
        $scope.currentPage = 1;
        $scope.updateTabel = true;

        orderApp.getItems = function(){
            $scope.updateTabel = true;
            $http({
                method: 'POST',
                url: orderApp.action,
                data: $scope.form ,
            }).success( function( data ) {
                orderApp.items = data.data;
                $scope.updateTabel = false;
                $scope.lastPage = data.last_page;
                if( data.last_page < $scope.currentPage ){
                    if( data.last_page == 0 )
                        $scope.currentPage = 1;
                    else
                        $scope.currentPage = data.last_page;
                }
            });
        }
        orderApp.getItems();

        $scope.changeInput = function( name ){
            $scope.currentPage = 1;
            $scope.form.page = 1;
            orderApp.getItems();
        }

        $scope.submit = function() {
            orderApp.getItems();
        }

        $scope.changeForm = function(){
            console.log('change');
        }

        $scope.changePage = function( num ){
            $scope.currentPage = num;
            $scope.form.page = num;
            orderApp.getItems();
        }

        $scope.sortBy = function( propertyName ) {
            if( $scope.form.sorts[propertyName] == undefined )
                $scope.form.sorts[propertyName] = 0;
            else if( $scope.form.sorts[propertyName] == 0 )
                $scope.form.sorts[propertyName] = 1;
            else
                delete $scope.form.sorts[propertyName];

            orderApp.getItems();
        };

        $scope.getNumber = function(start, to) {
            var arr = new Array();
            for( var i=start; i <= to; i++ ){
                arr.push(i);
            }
            return arr;
        }

    }).config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });