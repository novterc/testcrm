angular.module( 'goodsApp', [] )
    .controller( 'GoodsController', function( $scope, $http ) {
        var goodsApp = this;

        goodsApp.action = angular.element($('#goods-search-form')).attr('action');
        goodsApp.items = [];

        $scope.form = {
            paginate: '100',
            page: 1,
            sorts: {},
        };
        $scope.lastPage = 0;
        $scope.currentPage = 1;
        $scope.updateTabel = true;

        goodsApp.getItems = function(){
            $scope.updateTabel = true;
            $http({
                method: 'POST',
                url: goodsApp.action,
                data: $scope.form ,
            }).success( function( data ) {
                goodsApp.items = data.data;
                $scope.updateTabel = false;
                $scope.lastPage = data.last_page;
                if( data.last_page < $scope.currentPage )
                    $scope.currentPage = data.last_page;
            });
        }
        goodsApp.getItems();

        $scope.changeInput = function( name ){
            if( name == 'paginate' )
                $scope.form.page = 1;
            goodsApp.getItems();
        }

        $scope.submit = function() {
            goodsApp.getItems();
        }

        $scope.changeForm = function(){
            console.log('change');
        }

        $scope.changePage = function( num ){
            $scope.currentPage = num;
            $scope.form.page = num;
            goodsApp.getItems();
        }

        $scope.sortBy = function( propertyName ) {
            if( $scope.form.sorts[propertyName] == undefined )
                $scope.form.sorts[propertyName] = 0;
            else if( $scope.form.sorts[propertyName] == 0 )
                $scope.form.sorts[propertyName] = 1;
            else
                delete $scope.form.sorts[propertyName];

            goodsApp.getItems();
        };

        $scope.getNumber = function(start, to) {
            var arr = new Array();
            for( var i=start; i <= to; i++ ){
                arr.push(i);
            }
            return arr;
        }

    }).config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });
    