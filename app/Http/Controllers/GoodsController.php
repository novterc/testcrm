<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Good;
use App\Models\Advert;
use Validator;

class GoodsController extends Controller
{
    public function getList()
    {
        return view('twigs.pages.goodsList');
    }

    public function getJson(Request $request)
    {
        $goods = Good::findByDifferent($request->all());
        $goodsData = $goods->toArray();
        return response()->json( $goodsData );
    }

    public function getEdit(Request $request, $errors = [] )
    {
        $good = Good::find($request->id);
        if( $good === null ){
            return 'undefined';
        }
        $adverts = Advert::all();
        return view('twigs.pages.goodEdit', [
            'good' => $good,
            'adverts' => $adverts,
            'errors' => $errors,
            'sessionToken' => \Session::token(),
        ]);
    }

    public function saveEdit(Request $request)
    {
        $good = Good::find($request->id);
        if( $good === null ){
            return 'undefined';
        }

        $rules = [
            'good_advert' => 'required|integer|exists:adverts,user_id',
            'good_name' => 'required|string|max:255',
            'good_price' => 'required|integer|max:99999999999',
        ];
        if( $request->id !== $request->good_id )
            $rules['good_id'] = 'required|integer|min:1|max:99999999999|unique:goods';

        $validator = Validator::make( $request->all(), $rules);
        $messages = $validator->messages()->all();
        if( !$validator->fails() ){
            $good->good_id = $request->good_id;
            $good->good_advert = $request->good_advert;
            $good->good_name = $request->good_name;
            $good->good_price = $request->good_price;
            $good->save();
        }
        if( (int)$request->id !== (int)$good->good_id ){
            return redirect()->action('GoodsController@getEdit', [$good->good_id]);
        }

        return $this->getEdit($request, $messages);
    }
}
