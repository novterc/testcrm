<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Order;
use App\Models\State;

class OrdersController extends Controller
{
    public function getList()
    {
        $states = State::all();

        return view('twigs.pages.ordersList', [
            'states' => $states,
        ]);
    }

    public function getJson(Request $request)
    {   
        $orders = Order::findByDifferent( $request->all() );
        $ordersData = $orders->toArray();
        return response()->json( $ordersData );
    }

    public function testTwig()
    {
    }
}
