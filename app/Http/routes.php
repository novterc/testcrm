<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {
    
	Route::get('/login', 'AuthCartalystController@login');
	Route::post('/login', 'AuthCartalystController@loginProcess');
	Route::get('/logout', 'AuthCartalystController@logoutuser');

	// Route::group(['middleware' => 'auth.cartalyst'], function(){
		Route::get('/', 'OrdersController@getList');
		Route::get('/testTwig', 'OrdersController@testTwig');
		Route::post('/orders/getJson', 'OrdersController@getJson');

		Route::get('/goods', 'GoodsController@getList');
		Route::post('/goods/getJson', 'GoodsController@getJson');

		Route::get('/good-edit/{id}', 'GoodsController@getEdit');
		Route::post('/good-edit/{id}', 'GoodsController@saveEdit');
	// });

});