<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Advert extends Model
{
    protected $table = 'adverts';
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'user_first_name',
        'user_last_name',
        'user_login',
    ];

    protected $hidden = [
        'user_password',
    ];

    public function goods()
    {
        return $this->hasMany('App\Models\Good', 'good_advert', 'user_id');
    }
}
