<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
    protected $table = 'goods';
    protected $primaryKey = 'good_id';
    public $timestamps = false;

    protected $fillable = [
        'good_id',
        'good_name',
        'good_price',
        'good_advert',
    ];

    public function advert()
    {
        return $this->belongsTo('App\Models\Advert', 'good_advert', 'user_id');
    }

    // public static function findByDifferent( $confArray = [] )
    // {    
    //     $goods = new static;
    //     $fields = $goods->fillable;

    //     $goods = $goods->join('adverts', 'adverts.user_id', '=', 'good_advert');

    //     $fields['goods.good_name'] = 'good';
    //     if( !empty($confArray['sorts']) && is_array($confArray['sorts'])){
    //         foreach ( $confArray['sorts'] as $fieldName => $sort ) {
    //             $key = array_search( $fieldName, $fields );
    //             $sortName = ( boolval($sort) ? 'desc' : 'asc' );
    //             if( $key !== false ){
    //                 $field = $fields[$key];
    //                 $goods = $goods->orderBy($field, $sortName );
    //             }
    //         }
    //     }

    //     if( !empty( $confArray['paginate'] ) )
    //         $paginate = intval( $confArray['paginate'] );
    //     else
    //         $paginate = 100;
    //     $goods = $goods->paginate($paginate);

    //     return $goods;
    // }

    public static function findByDifferent( $confArray = [] )
    {    
        $goods = new static;
        $sortFields = $goods->fillable;
        $sortFields[] = 'user_first_name';

        $goods = $goods->join('adverts', 'adverts.user_id', '=', 'good_advert');
        $goods = $goods->select('*', 'adverts.*');

        if( !empty($confArray['sorts']) && is_array($confArray['sorts'])){
            foreach ( $confArray['sorts'] as $fieldName => $sort ) {
                $key = array_search( $fieldName, $sortFields );
                $sortName = ( boolval($sort) ? 'desc' : 'asc' );
                if( $key !== false ){
                    $field = $sortFields[ $key ];
                    $goods = $goods->orderBy($field, $sortName );
                }
            }
        }

        if( empty( $confArray['paginate'] ) )
            $confArray['paginate'] = 100;
        $goods = $goods->paginate($confArray['paginate']);

        return $goods;
    }
}
