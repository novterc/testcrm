<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';
    protected $primaryKey = 'state_id';
    public $timestamps = false;

    protected $fillable = [
        'state_id',
        'state_name',
        'state_slug',
    ];

    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'order_state', 'state_id');
    }
}
