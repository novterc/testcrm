<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'order_id';
    public $timestamps = false;

    protected $fillable = [
        'order_id',
        'order_state',
        'order_add_time',
        'order_good',
        'order_client_phone',
        'order_client_name',
    ];

    public function state()
    {
        return $this->belongsTo('App\Models\State', 'order_state', 'state_id');
    }

    public function good()
    {
        return $this->belongsTo('App\Models\Good', 'order_good', 'good_id');
    }


    public static function findByDifferent( $confArray = [] )
    {    
        $orders = new static;
        $sortFields = $orders->fillable;
        $sortFields[] = 'good_name';

        $allFieldsSearch = $sortFields;
        $exception = [ 'order_add_time', 'order_state' ];
        foreach ($exception as $expField) {
            if(($key = array_search( $expField, $allFieldsSearch)) !== false) {
                unset($allFieldsSearch[$key]);
            }
        }

        $orders = $orders->join('goods', 'orders.order_id', '=', 'goods.good_id');
        $orders = $orders->join('states', 'states.state_id', '=', 'order_state');
        $orders = $orders->join('adverts', 'adverts.user_id', '=', 'goods.good_advert');
        $orders = $orders->select('*', 'goods.*', 'states.*', 'adverts.*');

        if( !empty($confArray['order_id']) ){
            $orders = $orders->where('order_id', '=', $confArray['order_id']);
        }

        if( !empty($confArray['state']) ){
            $orders = $orders->where('order_state', '=', $confArray['state'] );
        }

        if( !empty($confArray['start']) ){
            $orders = $orders->where('order_add_time', '>=', $confArray['start']);
        }

        if( !empty($confArray['stop']) ){
            $orders = $orders->where('order_add_time', '<=', $confArray['stop'] );
        }

        if( !empty($confArray['phone']) ){
            $orders = $orders->where('order_client_phone', 'like',  '%' .$confArray['phone']. '%' );
        }

        if( !empty($confArray['good_name']) ){
            $orders = $orders->where('goods.good_name', 'like',  '%' .$confArray['good_name']. '%' );
        }
        
        if( !empty($confArray['sorts']) && is_array($confArray['sorts'])){
            foreach ( $confArray['sorts'] as $fieldName => $sort ) {
                $key = array_search( $fieldName, $sortFields );
                $sortName = ( boolval($sort) ? 'desc' : 'asc' );
                if( $key !== false ){
                    $field = $sortFields[ $key ];
                    $orders = $orders->orderBy($field, $sortName );
                }
            }
        }

        if( !empty($confArray['search']) ){
            $first = true;
            foreach ($allFieldsSearch as $field) {
                if( $first ){
                    $method = 'where';
                    $first = false;
                } else{
                    $method = 'orWhere';
                }
                $orders = $orders->$method($field, 'like',  '%' .$confArray['search']. '%' );
            }
        }

        if( empty( $confArray['paginate'] ) )
            $confArray['paginate'] = 100;
        $orders = $orders->paginate($confArray['paginate']);

        return $orders;
    }
}
