<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>@yield('title', 'testCRM')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @yield('styles')
        @yield('scripts')

    </head>
    <body>
        @yield('body')
        @yield('scriptsBottom')
    </body>
</html>