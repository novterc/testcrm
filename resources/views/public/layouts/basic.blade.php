@extends('basic')
@section('title', 'testCRM')
@section('styles')
    @parent
    <link href="/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
@stop
@section('scriptsBottom')
    <script src="/js/jquery.min.js"></script>
    <script src="/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
@stop