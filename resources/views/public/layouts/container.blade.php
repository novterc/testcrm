@extends('public.layouts.basic')
@section('body')

    @include('public.sections.navbar')
    
    <div class="container">
        @yield('container')
    </div>

@stop
