@extends('public.layouts.container')
@section('title')@parent-orders @stop
@section('styles')
    @parent
    <link href="/css/basic.css" rel="stylesheet">
@stop
@section('scriptsBottom')
    @parent
    <script src="/js/angular.min.js"></script>
    <script src="/js/ordersController.js"></script>
@stop
@section('container')
    
    <div ng-app="ordersApp" ng-controller="OrdersController as orderApp" >

        <h2>Заказы</h2>
        
        <form id="orders-search-form" action="{{URL::action('OrdersController@getJson')}}" onsubmit="return false;" ng-submit="submit()">
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="order_id">ID заказа:</label>
                        <input type="text" class="form-control" name="order_id" ng-model="form.order_id" ng-change="changeInput();" >
                    </div>
                    <div class="form-group">
                        <label for="interval-start">Интервал:</label>
                        <input type="text" class="form-control" name="start" id="interval-start" placeholder="От" ng-model="form.start" ng-change="changeInput();" >
                        <input type="text" class="form-control" name="stop" placeholder="До" ng-model="form.stop" ng-change="changeInput();" >
                    </div>
                    <div class="form-group">
                        <label for="state">Статус:</label>
                        <select class="form-control" id="state" name="state" ng-model="form.state" ng-change="changeInput();" >
                            <option value="0" ></option>
                            @foreach ( $states as $state)
                                <option value="{{$state->state_id}}" >{{$state->state_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="phone">Телефон:</label>
                        <input type="text" class="form-control" name="phone" ng-model="form.phone" ng-change="changeInput();" >
                    </div>
                    <div class="form-group">
                        <label for="good_name">Товар:</label>
                        <input type="text" class="form-control" name="good_name" ng-model="form.good_name" ng-change="changeInput();">
                    </div>
                    <div class="form-group">
                        <label for="search">Поиск:</label>
                        <input type="text" class="form-control" name="search" ng-model="form.search" ng-change="changeInput();">
                    </div>
                    <div class="form-group">
                        <label for="paginate">Показывать по:</label>
                        <select class="form-control" name="paginate" ng-model="form.paginate" ng-change="changeInput('paginate');">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>

        <ul class="pagination pagination-lg hide-block"  ng-class="{ 'show-block':1 }" ng-show="lastPage > 1">
          <li ng-repeat="i in getNumber(1, lastPage)" 
                ng-class="{'active': currentPage == i}" 
                ng-click="changePage( i )"><a ><% i %></a></li>
        </ul>

        <br/><br/>

        <table class="table hide-block" id="order-table" ng-class="{ 'update-table': updateTabel, 'show-block':1 }">
            <thead>
                <tr>
                    @foreach(  [
                        'order_id' => '#',
                        'order_add_time' => 'Дата',
                        'order_client_name' => 'Клиент',
                        'order_client_phone' => 'Телефон',
                        'good_name' => 'Товар',
                        'order_state' => 'Статус',
                    ] as $sortKey => $title )
                     <th class="my-sorts" 
                        ng-click="sortBy('{{$sortKey}}')" 
                        ng-class="{ 'sort': form.sorts.{{$sortKey}} != undefined , reverse: form.sorts.{{$sortKey}} == 1}">{{$title}}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in orderApp.items">
                        <td><% item.order_id %></td>
                        <td><% item.order_add_time %></td>
                        <td><% item.order_client_name %></td>
                        <td><% item.order_client_phone %></td>
                        <td>
                            <a href="{{URL::action('GoodsController@getEdit',[null])}}/<% item.good_id %>"><% item.good_name %></a><br/>
                            <% item.user_first_name %> 
                            <% item.user_last_name %> 
                            ( <% item.user_login %> )
                        </td>
                        <td><% item.state_name %></td>
                    </tr>    
            </tbody>
        </table>


        <br/>

        <ul class="pagination pagination-lg hide-block" ng-class="{ 'show-block':1 }" ng-show="lastPage > 1">
          <li ng-repeat="i in getNumber(1, lastPage)" 
                ng-class="{'active': currentPage == i}" 
                ng-click="changePage( i )"><a ><% i %></a></li>
        </ul>

        <br/>
        <br/>
        <br/>
    </div>

@stop
