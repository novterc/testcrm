@extends('public.layouts.container')
@section('title')@parent-order edit @stop
@section('container')
    
    <h2>Товары</h2>

    @if( !empty($errors) && is_array($errors) )
        @foreach ( $errors as $error)
            <div class="alert alert-danger">
                <strong>ERROR!</strong> {{$error}}
            </div>
        @endforeach
    @endif

    <form id="orders-search-form" method="POST" >
        <input type="hidden" name="_token" value="{{ Session::token() }}" />
        <div class="form-group">
            <label for="order_id">ID заказа:</label>
            <input type="text" class="form-control" name="good_id" value="{{$good->good_id}}" >
        </div>
        <div class="form-group">
            <label for="advert">Рекламодатель:</label>
            <select class="form-control" id="advert" name="good_advert" >
                <option value="0" ></option>
                @foreach ( $adverts as $advert)
                    <option value="{{$advert->user_id}}" {{( $advert->user_id == $good->good_advert ? 'selected="selected"':'' )}} >{{$advert->user_first_name}} {{$advert->user_last_name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="phone">Название:</label>
            <input type="text" class="form-control" name="good_name" value="{{$good->good_name}}" >
        </div>
        <div class="form-group">
            <label for="phone">Цена:</label>
            <input type="text" class="form-control" name="good_price" value="{{$good->good_price}}">
        </div>
        <button type="submit" class="btn btn-default">Сохранить</button>
    </form>

@stop
