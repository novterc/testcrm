<nav class="navbar navbar-default ">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{URL::action('OrdersController@getList')}}" ><span class="glyphicon glyphicon-home"></span></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                @foreach ( [
                    URL::action('OrdersController@getList') => 'Заказы',
                    URL::action('GoodsController@getList') => 'Товары',
                ] as $href => $name)
                    <li <?=($name === @$selectNavItem ? 'class="active"' : '')?>><a href="{{$href}}">{{$name}}</a></li>
                @endforeach
            </ul>
            <div class="nav navbar-nav navbar-right">
                <a class="navbar-brand" title="Log out" href="/logout" ><span class="glyphicon glyphicon-log-out"></span></a>
            </div>
        </div>
    </div>
</nav>