<?php

use Illuminate\Database\Seeder;

class AdvertsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$table = DB::table('adverts');
        $table->insert([
            'user_first_name' => 'John',
            'user_last_name' => 'Doe',
            'user_login' => 'jdoe@gmail.com',
            'user_password' => 'password',
        ]);
        $table->insert([
            'user_first_name' => 'Erick',
            'user_last_name' => 'Doe',
            'user_login' => 'edoe@gmail.com',
            'user_password' => 'password',
        ]);
        $table->insert([
            'user_first_name' => 'Michel',
            'user_last_name' => 'Doe',
            'user_login' => 'mdoe@gmail.com',
            'user_password' => 'password',
        ]);
    }
}
