<?php

use Illuminate\Database\Seeder;

class GoodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$table = DB::table('goods');
        $table->insert([
            'good_name' => 'Часы Rado Integral',
            'good_price' => 2000,
            'good_advert' => 1,
        ]);
        $table->insert([
            'good_name' => 'Часы Swiss Army',
            'good_price' => 1500,
            'good_advert' => 1,
        ]);
        $table->insert([
            'good_name' => 'Детский планшет',
            'good_price' => 2100,
            'good_advert' => 2,
        ]);
        $table->insert([
            'good_name' => 'Колонки Monster Beats',
            'good_price' => 900,
            'good_advert' => 3,
        ]);
    }
}
