<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call( RoleSeeder::class );
        // $this->call( UsersTableSeeder::class );
        $this->call( AdvertsTableSeeder::class );
        $this->call( GoodsTableSeeder::class );
        $this->call( OrdersTableSeeder::class );
        $this->call( StatesTableSeeder::class );
    }
}
