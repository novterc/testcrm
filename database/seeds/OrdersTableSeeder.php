<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = DB::table('orders');
        $table->insert([
            'order_state' => 1,
            'order_add_time' => '2016-01-01 00:01:00',
            'order_good' => 1,
            'order_client_phone' => 777713522547,
            'order_client_name' => 'John',
        ]);
        $table->insert([
            'order_state' => 2,
            'order_add_time' => '2016-01-01 00:02:00',
            'order_good' => 2,
            'order_client_phone' => 77756547656,
            'order_client_name' => 'Michel',
        ]);
        $table->insert([
            'order_state' => 3,
            'order_add_time' => '2016-01-02 00:01:00',
            'order_good' => 3,
            'order_client_phone' => 77786789878,
            'order_client_name' => 'Darrel',
        ]);
        $table->insert([
            'order_state' => 1,
            'order_add_time' => '2016-03-02 00:01:00',
            'order_good' => 4,
            'order_client_phone' => 77752456523,
            'order_client_name' => 'Dan',
        ]);
    }
}
