<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$table = DB::table('states');
        $table->insert([
            'state_name' => 'Новый',
            'state_slug' => 'new',
        ]);
        $table->insert([
            'state_name' => 'В работе',
            'state_slug' => 'onoperator',
        ]);
        $table->insert([
            'state_name' => 'Подтвержден',
            'state_slug' => 'accepted',
        ]);
    }
}
